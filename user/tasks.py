from time import sleep

from celery.schedules import crontab
from celery.task import task, periodic_task
from datetime import datetime

@task(name='send time')
def send_time():
    sleep(10)
    return datetime.now()


@periodic_task(name='send_time_periodic', run_every=crontab(minute='*'))
def send_time2():
    return 'datetime.now{}'.format(datetime.now())