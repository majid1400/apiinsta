from django.urls import path
from django.views.generic import TemplateView
from user.views import RegisterViews, LoginViews, ProfileUpdate

urlpatterns = [
    path('register/',RegisterViews.as_view(), name='register'),
    path('login/',LoginViews.as_view(), name='login'),
    path('profileupdate/',ProfileUpdate.as_view(), name='profileupdate'),
]
