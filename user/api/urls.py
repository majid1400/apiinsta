from django.urls import path
from user.api.views import ProfileRetrieveAPIView, ProfileRetrieveUpdateAPIView, ProfileListAPIView
from user.views import ProfileDetail

urlpatterns = [
    path('profile/<str:username>', ProfileRetrieveAPIView.as_view(), name=''),
    path('listprofile', ProfileListAPIView.as_view(), name=''),
    path('profile/', ProfileRetrieveUpdateAPIView.as_view(), name=''),

]