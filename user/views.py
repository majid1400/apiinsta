from django.contrib.auth import get_user_model, login
from django.http import HttpResponse
from django.views.generic import FormView, UpdateView, DetailView

from relation.models import Relation
from user.forms import RegistrationForm, LoginForm


User = get_user_model()

class RegisterViews(FormView):

    form_class = RegistrationForm
    template_name = 'user/register.html'
    success_url = '/'
    
    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
    
class LoginViews(FormView):
    
    form_class = LoginForm
    template_name = 'user/login.html'
    success_url = '/'
    
    def form_valid(self, form):
        login(self.request, form.cleaned_data['user'])
        return super().form_valid(form)
    

class ProfileUpdate(UpdateView):
    model = User
    fields = ('username', 'email', 'bio', 'website', 'avatar', 'phone_number')
    template_name = 'user/profile_update.html'
    success_url = '/'
    
    def get_object(self, queryset=None):
        return self.request.user

class ProfileDetail(DetailView):
    model = User
    slug_url_kwarg = 'username'
    slug_field = 'username'
    template_name = 'user/profile.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context['post_conut'] = user.posts.count()
        context['followings_conut'] = user.followings.count()
        context['followers_conut'] = user.followers.count()
        context['is_follow'] = Relation.objects.filter(from_user=self.request.user, to_user=user).exists()
        return context