from django.urls import path

from activity.views import CommentCreateAPIViews

urlpatterns = [
    path('comment/', CommentCreateAPIViews.as_view(), name='')
]