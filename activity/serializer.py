from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from activity.models import Comment
from relation.models import Relation


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('caption','post','reply_to', 'user')
        extra_kwargs = {'user': {'read_only': True}}
    
    # اگر api که فرستاده میشه را بخواهیم چک کنیم تابع زیر را دوباره نویسی میکنیم
    def validate(self, attrs):
        # attrs.get("reply_to")     => <Comment: frist comment> کپشن کامنتی که می خواهی پاسخ بدی چی هست؟
        # attrs["reply_to"].post    => <Post: beh (1)> زیر کامنت کدوم پست می خواهی پاسخ بدی؟
        # attrs["post"]             => <Post: beh (1)>   کامنت برای کدوم پست؟
        # attrs['post'].user        => <User: beh>     نام کاربری پستی که می خواهی کامنت بگذاری چی هست؟
        
        request = self.context['request']
        
        if attrs.get("reply_to") is not None and attrs["reply_to"].post != attrs["post"]:
            raise ValidationError(_("Post and comment post are not the same"))
        
        if request.user != attrs['post'].user and not Relation.objects.filter(
            from_user=request.user, to_user=attrs['post'].user
        ).exists():
            raise ValidationError(_("You are not allowed to do this action"))
        
        return attrs