from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

# Create your views here.
from activity.models import Comment
from activity.serializer import CommentSerializer


class CommentCreateAPIViews(generics.CreateAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    
    # قبل از نمایش داده به کار و ذخیره کردن در دیتابیس میاد یوزر را نمایش میده
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    
    
