from __future__ import absolute_import, unicode_literals
# از همین فایل سلری که در بالا ساختیم اپ را بعنوان سلری اپ ایمپورت می کنیم و داخل می نویسیم که سلری اپ قابل ایمپورت کردند باشه از کل برنامه مون که یک کار ثابت هست
from .celery import app as celery_app
__all__ = ('celery_app',)