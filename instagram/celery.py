from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
# به جنگو میگیم که تنظیمات ش را بره از تنظیمات خود اپلیکیشن مون بخونه
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'instagram.settings')
# یک اپ جدید برای سلیری می سازیم
app = Celery('instagram')
# اینا ثابت هستند، بهش دست نمی زنیم
app.config_from_object('django.conf:settings', namespace='CELERY')
# به ما اجازه میده در هر اپی در پروژه مون تسک مربوط به همون اپ را بنویسیم
# میره تو همه فولدرها دنبال فایلی به نام تسک می گرده
app.autodiscover_tasks()