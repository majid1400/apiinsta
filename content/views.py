from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import generics

from content.models import Post
from content.serializers import PostListSerializer, PostCreateSerializer


class PostListCreateAPIView(generics.ListCreateAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    
    def get_queryset(self):
        gs = super().get_queryset()
        return gs.filter(user=self.request.user)
    
    # اگر دوتا سریلایزر داشتیم این متد را دوباره نویسی می کنیم
    def get_serializer_class(self):
        if self.request.method.lower() ==  "post":
            return PostCreateSerializer
        return self.serializer_class