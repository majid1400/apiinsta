from django.urls import path

from content.views import PostListCreateAPIView

urlpatterns = [
    path('list/', PostListCreateAPIView.as_view(), name=''),

]