from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


from lib.common_models import BaseModel
from location.models import Location

User = get_user_model()

class Post(BaseModel):
    caption = models.TextField(_('caption'), blank=True)
    user = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    location = models.ForeignKey(Location, related_name='posts', on_delete=models.CASCADE, blank=True)
    
    def __str__(self):
        return "{} ({})".format(self.user.username, self.id)

class PostMedia(BaseModel):
    IMAGE = 0
    VIDEOS = 1
    MEDIA_TYPE = (
        (IMAGE, _('Image')),
        (VIDEOS, _('Video')),
    )
    media_type = models.PositiveSmallIntegerField(_('media type'), choices=MEDIA_TYPE, default=IMAGE)
    post = models.ForeignKey(Post, related_name='media', on_delete=models.CASCADE)
    media_file = models.FileField(_('media file'),
                                  upload_to='content/media/',
                                  validators=[FileExtensionValidator(allowed_extensions=('png'))])
    
    def __str__(self):
        return '{} - {}'.format(str(self.post), self.get_media_type_display())

class Tage(BaseModel):
    title = models.CharField(_('title'), max_length=32)

class PostTag(BaseModel):
    post = models.ForeignKey(Post, related_name='hashtags', on_delete=models.CASCADE)
    tag = models.ForeignKey(Tage, related_name='postTags', on_delete=models.CASCADE)

class TaggeUser(BaseModel):
    user = models.ForeignKey(User, related_name='tagUseras', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='tagUsers', on_delete=models.CASCADE)