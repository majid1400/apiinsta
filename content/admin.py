from django.contrib import admin

# Register your models here.
from django.contrib.admin import register

from content.models import Post, PostMedia, Tage, PostTag, TaggeUser



class PostMediaInline(admin.TabularInline):
    model = PostMedia

class TaggeUserInline(admin.TabularInline):
    model = TaggeUser

class PostTagInline(admin.TabularInline):
    model = PostTag

@register(Tage)
class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'create_time')
    
    
@register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('user', 'caption', 'location')
    inlines = (PostMediaInline,PostTagInline,TaggeUserInline)

