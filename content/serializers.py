from rest_framework import serializers

from content.models import Post, PostMedia, TaggeUser
from location.serializer import LocationListSerializer
from user.api.serializers import UserPostSerializers


class MediaListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostMedia
        fields = ('media_type','media_file')

class TaggedUserPostCreateSerializers(serializers.ModelSerializer):
    user = UserPostSerializers()
    class Meta:
        model = TaggeUser
        fields = ("user",)

class PostListSerializer(serializers.ModelSerializer):
    user = UserPostSerializers()
    location = LocationListSerializer()
    media = MediaListSerializer(many=True)
    tagUsers = TaggedUserPostCreateSerializers(many=True)
    
    class Meta:
        model = Post
        fields = ("id","caption", "user","location","media","tagUsers",)




class PostCreateSerializer(serializers.ModelSerializer):
    tagUsers = TaggedUserPostCreateSerializers()

    class Meta:
        model = Post
        fields = ("caption", "location", "tagUsers",)