from django.urls import path

from relation.views import FollowView, FollowersListApiView

urlpatterns = [
    path('<str:username>/follow', FollowView.as_view(), name='followUnFollow'),
    path('FollowersList/', FollowersListApiView.as_view(), name='')
]