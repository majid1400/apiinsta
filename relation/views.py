from django.contrib.auth import get_user_model
from django.db import transaction
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from rest_framework import generics, serializers

# Create your views here.
from django.views import View

from relation.models import Relation

User = get_user_model()

class FollowView(View):
    
    def get_object(self):
        try:
            user = User.objects.get(username=self.kwargs.get('username'))
        except User.DoesNotExist:
            raise Http404
        return user
        
    def post(self, request, *args, **kwargs):
        target_user = self.get_object()
        
        if target_user == request.user:
            return redirect('/{}/'.format(target_user))
        
        qs = Relation.objects.filter(from_user = request.user, to_user=target_user)
        if qs.exists():
            qs.delete()
        else:
            with transaction.atomic():
                Relation.objects.create(from_user = request.user, to_user=target_user)
        return redirect('/{}/'.format(target_user))



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username",)


class RelationSerializer(serializers.ModelSerializer):
    to_user = UserSerializer()
    
    class Meta:
        model = Relation
        fields = ("from_user",)
        
        
class FollowersListApiView(generics.ListAPIView):
    queryset = Relation.objects.select_related('to_user').all()
    serializer_class = RelationSerializer
    
    def get_queryset(self):
        gs = super().get_queryset()
        return gs.filter(from_user=self.request.user)