from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import gettext_lazy as _
from lib.common_models import BaseModel


class Location(BaseModel):
    title = models.CharField(_('title'), max_length=32)
    points = JSONField(_('points'))
    
    def __str__(self):
        return self.title
