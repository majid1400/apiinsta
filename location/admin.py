from django.contrib import admin

# Register your models here.
from django.contrib.admin import register

from location.models import Location


@register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('title', 'points')