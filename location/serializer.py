from rest_framework import serializers

from location.models import Location


class LocationListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location
        fields = ("title",'points')